extern string_equals

global find_word

section .text

find_word:

.loop:
    push rsi
    push rdi
 
    add rsi, 8  
    call string_equals

    pop rdi
    pop rsi

    test rax, rax
    jnz .found
    mov rsi, [rsi] 
    test rsi, rsi
    jz .not_found
    jmp .loop

.found:
    mov rax, rsi
    jmp .end 

.not_found: 
    xor rax, rax

.end: 
    ret  
