section .text

global exit
global string_length
global print_info
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_err 

%define stdin 0
%define stdout 1
%define stderr 2

%define sys_print 1
%define sys_exit 60

%define newline_sym 0xA

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, sys_exit  
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop: 
    cmp byte [rdi + rax], 0
    je .end
    inc rax 
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
    call string_length
	pop rdi
    mov rsi, rdi
    mov rdi, stdout
    mov rdx, rax
    mov rax, sys_print
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_info:  
    mov rsi, stdout
    call print_string
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_err:
    mov rsi, stderr
    call print_string
    ret

; Принимает код символа, номер дескриптора 
print_char:
    push rdi
    mov rdi, stdout
    mov rsi, rsp
    mov rdx, 1
    mov rax, sys_print
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline: 
    mov rdi, newline_sym
    push rdi
    call print_char
    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.

%define divider 10
%define ascii_number_border 48
print_uint:
    mov rax, rdi
    mov rbx, divider
    push 0

.loop:
    xor rdx, rdx
    div rbx
    add rdx, ascii_number_border
    push rdx
    test rax, rax
    jne .loop

.get_number:
    pop rdi
    test rdi, rdi
    jne .next_step
    ret

.next_step:
    call print_char
    jmp .get_number
 

%define minus_sym '-'
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .unsigned_number
    neg rdi
    push rdi
    mov rdi, minus_sym
    call print_char
    pop rdi
.unsigned_number: 
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    call string_length
    mov rdx, rax
    xchg rdi, rsi
    call string_length 
    cmp rax, rdx                    
    jne .unequals
    mov rcx, 0 

.loop:
    cmp rcx, rax
    je .equals
    mov dl, byte [rsi + rcx]  
    cmp byte [rdi + rcx], dl
    jne .unequals
    inc rcx            
    jmp .loop

.equals:     
    mov rax, 1
    jmp .end

.unequals: 
    mov rax, 0
.end:
    ret

%define null 0
%define amount_of_symbols 1
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    mov rdi, stdin
    mov rdx, amount_of_symbols
    push null 
    mov rsi, rsp
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор 
%define space_sym 0x20
%define tab_sym 0x9
read_word:
    xor rax, rax
    xor rcx, rcx 
.loop:
    cmp rcx, rsi
    jae .not_enough_space
    push rdx
    push rcx
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    pop rcx
    pop rdx
    cmp al, newline_sym
    je .process_sym
    cmp al, tab_sym  
    je .process_sym
    cmp al, space_sym
    je .process_sym
    cmp al, byte null
    je .end
    mov [rdi + rcx], al
    inc rcx
    jmp .loop

.not_enough_space: 
    xor rax, rax
    ret

.process_sym:
    test rcx, rcx
    je .loop

.end:
    mov [rdi + rcx], byte 0
    mov rax, rdi
    mov rdx, rcx
    ret
    
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
%define nine_ascii_code 57
%define zero_ascii_code 48
parse_uint:
    call string_length
    mov rsi, rax 
    xor rcx, rcx
    xor rdx, rdx
    xor rax, rax
    xor r8, r8 

.loop:
    mov r8b, byte [rdi + rcx]
    test r8b, r8b
    je .end
    cmp r8b, zero_ascii_code
    jb .end
    cmp r8b, nine_ascii_code
    ja .end
    sub r8b, zero_ascii_code
    imul rax, 10
    add rax, r8
    inc rdx
    inc rcx
    jmp .loop

.end:
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int: 
    xor rdx, rdx
    cmp byte [rdi], minus_sym
    jne .unsigned
    inc rdi 
    call parse_uint
    neg rax
    inc rdx               
    jmp .end

.unsigned:
    call parse_uint

.end:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    inc rax 
    cmp rax, rdx
    jg .not_enough_space
    mov rcx, 0

.loop: 
    cmp rcx, rax
    je .end
    mov r8b, byte [rdi + rcx]
    mov byte [rsi + rcx], r8b  
    inc rcx
    jmp .loop

.not_enough_space:
    mov rax, 0
    ret

.end:
    dec rax
    ret