main.o: main.asm words.inc colon.inc
	nasm -felf64  main.asm 

lib.o: lib.asm
	nasm -felf64 lib.asm 

dict.o: dict.asm
	nasm -felf64 dict.asm
	
main: main.o dict.o lib.o
	ld -o main main.o dict.o lib.o

clear:
	rm -f main *.o
