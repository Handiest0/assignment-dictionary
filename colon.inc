%ifndef node
    %define node 0
%endif 

%macro colon 2

        %ifid %2
             %2:
                dq node       
        %else 
            %error "error with label: the label is not an identifier" 
        %endif 

        %ifstr %1
            db %1, 0        
        %else
            %error "error with the key: the key is not a string value" 
        %endif


    %define node %2  

%endmacro
