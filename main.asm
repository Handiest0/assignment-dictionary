%define size 256

section .data
    %include "words.inc"
    not_found_msg: db 'Results not found.', 0
    buffer_err: db 'Buffer size is not enough', 0

extern find_word
extern read_word
extern print_info 
extern print_err
extern print_newline
extern exit

global _start

section .text

_start:
    sub rsp, size
    mov rdi, rsp 
    mov rsi, size 

    push rdi
    push rsi     
    call read_word
    pop rsi
    pop rdi

    test rax, rax
    jz .not_enough_space

    push rdx
    mov rsi, node
    call find_word
    test rax, rax
    jz .not_found 

    pop rdx
    add rax, 9
    add rax, rdx 
    mov rdi, rax
    call print_info
    call print_newline
    mov rdi, 0 
    jmp .end

.not_enough_space:
    mov rdi, buffer_err
    call print_err
    call print_newline
    jmp .end 

.not_found:
    mov rdi, not_found_msg 
    call print_err
    call print_newline
    mov rdi, 1 

.end:
    call exit 

    
